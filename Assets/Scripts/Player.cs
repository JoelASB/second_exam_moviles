﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Player : MonoBehaviour
{
    Rigidbody2D rb;
    //BoxCollider2D Colisionador;
    public Button _Swip_or_Accelerator;
    public bool swip_or_sccelerator;

    //SpriteRenderer sprite;


    public Text Contador_Puntos;
    public Text Contador_Vida;
    public float N_Points;
    public float N_Life;

    public int ForceAirSwip;
    public int ForceAirAccel;
    //float Left;
    //float Right;
    [Header("Desplazamineto de pantalla")]
    public Vector2 PointPressInit;
    public Vector2 PointPressFinal;
    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        rb.gravityScale = 0;
        //Colisionador = GetComponent<BoxCollider2D>();
        //sprite = GetComponent<SpriteRenderer>();
        N_Points = 0;
        N_Life = 10;
        swip_or_sccelerator = false;
        Contador_Puntos.text = "Points: " + N_Points.ToString();
        Contador_Vida.text = "Life: " + N_Life.ToString();

        _Swip_or_Accelerator.onClick.AddListener(() => Swip_or_Accelerator());

    }

    // Update is called once per frame
    void Update() {

        if (!swip_or_sccelerator) {
            if (Input.acceleration.y > 0.05f) { UpAccel(); }
            else if (Input.acceleration.y < -0.05f) { DownAccel(); }
        } else {
            if (Input.touchCount > 0)
            {
                Touch touch = Input.GetTouch(0);
                switch (touch.phase)
                {
                    case TouchPhase.Began:
                        PointPressInit = new Vector2(touch.position.x, touch.position.y);
                        break;

                    case TouchPhase.Ended:



                        PointPressFinal = new Vector2(touch.position.x, touch.position.y);
                        Vector2 Result = new Vector2(PointPressFinal.x - PointPressInit.x, PointPressFinal.y - PointPressInit.y);
                        Result.Normalize();
                        if (Result.y > 0 && Result.x > -0.5 && Result.x < 0.5) { UpSwip(); }
                        else if (Result.y < 0 && Result.x > -0.5 && Result.x < 0.5) { DownSwip(); }
                        break;
                }
            }
        }
        //Debug.Log(swip_or_sccelerator);
        if (N_Points == 30) {
            N_Points = 0;
            N_Life += 2;
            Contador_Vida.text = "Life: " + N_Life.ToString();
            Contador_Puntos.text = "Points: " + N_Points.ToString();
        }

    }

    void UpAccel()  {
        transform.position = new Vector2(transform.position.x, transform.position.y + ForceAirAccel * Time.deltaTime);
    }
    void DownAccel() {
        transform.position = new Vector2(transform.position.x, transform.position.y - ForceAirAccel * Time.deltaTime);
    }
    void UpSwip()
    {
        transform.position = new Vector2(transform.position.x, transform.position.y + ForceAirSwip * Time.deltaTime);
    }
    void DownSwip()
    {
        transform.position = new Vector2(transform.position.x, transform.position.y - ForceAirSwip * Time.deltaTime);
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("Snitch"))
        {
            N_Points+=10;
            Contador_Puntos.text = "Points: " + N_Points.ToString();
            Destroy(collision.gameObject);
        }

        if (collision.gameObject.CompareTag("Enemy"))
        {
            N_Life-=1;
            Contador_Vida.text = "Life: " + N_Life.ToString();
            Destroy(collision.gameObject);
        }
    }
    public void Swip_or_Accelerator() {  swip_or_sccelerator = !swip_or_sccelerator; }

    //private void OnTriggerEnter2D(Collider2D collision)
    //{
    //    if (collision.gameObject.CompareTag("Snitch"))
    //    {
    //        N_Points += 10;
    //        Contador_Puntos.text = "Points: " + N_Points.ToString();
    //        Destroy(collision.gameObject);
    //    }

    //    if (collision.gameObject.CompareTag("Enemy"))
    //    {
    //        N_Life -= 1;
    //        Contador_Vida.text = "Life: " + N_Life.ToString();
    //        Destroy(collision.gameObject);
    //    }
    //}


}
