﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Objeto_y_Enemigo : MonoBehaviour
{
    public float Speed;
    int _Direccion;

    SpriteRenderer Direccion_Sprite;

    // Start is called before the first frame update
    void Start()
    {
        Direccion_Sprite = GetComponent<SpriteRenderer>();
        Direccion();
    }

    // Update is called once per frame
    void Update()
    {
        transform.position = new Vector2(transform.position.x + Speed * _Direccion * Time.deltaTime, transform.position.y);
    }

    void Direccion() {
        if (transform.position.x < 0) {
            _Direccion = 1;
            Direccion_Sprite.flipX = false;
        } else {
            _Direccion = -1;
            Direccion_Sprite.flipX = true;
        }
    }
}
